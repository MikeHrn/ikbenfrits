# Ikbenfrits refactor case
The goal of this repo is to refactor the existing "FormStep" component.
Because I can't access other components/utils the structure in this repo is based on some assumptions (see notes in comments as well), let's discuss this together :) 

####  My concerns about the current FormStep component
* No comments, hard to understand for an outside/new developer
* It has too much logic that can be abstracted in multiple components
* Render logic is determined by the backend rather than the frontend
* Data is stored in the store even if it could maybe be local data
* All styling/positioning is fixed, what will happen when you want to have a variation that's slightly different?

####  What creates a good component
* Create flexible, 'dumb' components
* Components should receive data as props and emit events
* Local state should not be stored in the Vuex store
* Logic is handled by the parent component 
* When possible business logic should be abstracted (either as mixin, util, or prototype)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```