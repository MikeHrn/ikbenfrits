module.exports = {
  purge: { content: ['./public/**/*.html', './src/**/*.vue'] },
  darkMode: false,
  theme: {
    extend: {},
    fontFamily: {
      'sans': ['proxima-nova', 'Arial', 'sans-serif'],
    },
    colors: {
      blue: {
        100: '#DFECF0',
        300: '#54a1cc',
        900: '#365A76'
      },
      white: '#fff',
      grey: {
        300: '#e5e7eb'
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
