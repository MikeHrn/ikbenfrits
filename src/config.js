const CONFIG = {
  newsletter: {
    labels: {
      form: {
        paragraph: 'Leuk! Het enige wat ik van je nodig heb is een e-mailadres.',
      },
      feedback: {
        sentMessage: 'Je bent ingeschreven!',
        sentParagraph: '',
      },
    },
    leadmagnetProductType: 'newsletter',
    leadmagnetNewsletterCheckbox: true,
    hideCloseButton: true,
    hideExtraEmail: true,
    hideNewsletterCheckbox: true,
  },
  ebook_dream_home: {
    labels: {
      form: {
        heading: 'Download hier mijn gratis e-book over het bepalen van jouw openingsbod',
        paragraph: 'Je gaat een bod plaatsen op een huis? Spannend! Vul hier je e-mailadres in, dan stuur ik je gratis mijn e-book. Daarin leg ik je precies uit hoe dat nou moet, bieden op een huis. Ook vind je er een handige voorbeeldbrief. Die kun je gebruiken om de makelaar in te lichten over jouw bod!\n',
      },
      feedback: {
        sentMessage: 'E-book verstuurd!',
        sentParagraph: 'Check snel je inbox!',
      },
    },
    leadmagnetProductType: 'ebook',
    leadmagnetProduct: 'ebook_bieden_op_jouw_droomhuis.pdf',
  },
  save_dossier: {
    labels: {
      form: {
        heading: '',
        paragraph: 'Vul hier je e-mailadres in, dan slaan wij je gegevens veilig op in je online dossier. Je ontvangt een e-mail met daarin een PDF met je berekening.\n',
      },
      feedback: {
        sentMessage: 'Dossier aangemaakt!',
        sentParagraph: 'Dank je wel! Je online dossier is aangemaakt. Er is een e-mail naar je verstuurd. Check je inbox om naar je dossier te gaan.',
      },
    },
    leadmagnetProductType: 'save_data',
    leadmagnetProduct: 'save_dossier_refinance',
    // leadmagnetPersonalCheckbox: __CBHS__ ? undefined : true,
    // hideNewsletterCheckbox: __CBHS__,
    // hidePersonalCheckbox: !__CBHS__,
    // ^^ Commented out because CHBS is not defined
    hideExtraEmail: true,
    submitTitle: 'Gegevens bewaren',
  },
};

export default CONFIG;
