/**
 * Mixin for creating gated downloads.
 * The mixins gets the data from the API/config based on the lead key and transforms this
 * so it can be used with various types of UI-elements.
 */

import CONFIG from '@/config';

export default {
  props: {
    leadKey: {
      type: String,
      required: false,
    },
  },

  data() {
    return {
      data: {},
    };
  },

  created() {
    this.getFormData(this.leadKey);
  },

  computed: {
    /**
     * Return all labels
     * @return {Object} Object containing all labels
     */
    labels() {
      return this.data.labels;
    },
  },

  methods: {
    getFormData(leadKey) {
      // Normally this function would use this.$tool or some kind of API abstraction
      // depending on how data is being handled.
      // for now we just return the config file and pretend it came from the backend.
      this.data = CONFIG[leadKey];
    },
  },
};
