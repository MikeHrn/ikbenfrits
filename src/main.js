import Vue from 'vue';
import App from './App.vue';

import './assets/css/tailwind.css';
import './assets/css/classes/typography.css';
import './assets/css/classes/button.css';

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');
